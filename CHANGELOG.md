<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC0-1.0
-->

# Changelog

## v0.4.1: Patch for ClassApiInfo

This patch fixes a small issues to decode the api info on classes.

## v0.4.0: Initial version

This is the first official release with a revised API, documentation and test
suite. It is not backwards compatible to `v0.4.0.beta2` and in particular not
compatible with the implementation in [dasf-web@0.3.4][dasf-web@0.3.4].

## v0.4.0.beta2: Initial beta version

This is the initial version (beta) extracted from the [dasf-web@0.3.4][dasf-web@0.3.4]
package, and migrated to the [typescript-package-template][typescript-package-template].

[dasf-web@0.3.4]: https://codebase.helmholtz.cloud/dasf/dasf-web/-/releases/v0.3.4
[typescript-package-template]: https://codebase.helmholtz.cloud/hcdc/software-templates/typescript-package-template
