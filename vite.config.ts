// SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: CC0-1.0

import { defineConfig } from 'vite';
import { resolve } from 'path';
import dts from 'vite-plugin-dts';

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'src/main.ts'),
      name: 'dasf-messaging',
      fileName: 'dasf-messaging',
    },
  },
  resolve: { alias: { src: resolve('src/') } },
  plugins: [dts({ rollupTypes: true })],
});
