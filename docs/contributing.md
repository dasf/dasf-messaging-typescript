<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(contributing)=

# Contribution and development hints

The _dasf-messaging-typescript_ project is developed by the
Helmholtz-Zentrum Hereon[institution-url]. It is open-source
as we believe that this analysis can be helpful for reproducibility and
collaboration, and we are looking forward for your feedback,
questions and especially for your contributions.

- If you want to ask a question, are missing a feature or have
  comments on the docs, please [open an issue at the source code
  repository][issues]
- If you have suggestions for improvement, please let us know in an
  issue, or fork the repository and create a merge request. See also
  {ref}`development`.

[institution-url]: https://www.hereon.de

[issues]: https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript

(development)=

## Contributing in the development

```{warning}
We use automated formatters to ensure a high quality and maintanability of
our source code. Getting familiar with these techniques can take quite some
time and you might get error messages that are hard to understand.

We not slow down your development and we do our best to support you with
these techniques. If you have any troubles, just commit with
`git commit --no-verify` (see below) and the maintainers will take care
of the tests and continuous integration.
```

Thanks for your wish to contribute to this project!! The source code of
the _dasf-messaging-typescript_ package is hosted at https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript.

This is an open gitlab where you can register via the Helmholtz AAI. If your
home institution is not listed in the Helmholtz AAI, please use one of the
social login providers, such as Google, GitHub or OrcID.

Once you created an account in this gitlab, you can [fork][fork] this
repository to your own user account and implement the changes.

Afterwards, please make a merge request into the main repository. If you
have any questions, please do not hesitate to create an issue on gitlab
and contact the maintainers of this package.

Once you created you fork, you can clone it via

```bash
git clone https://codebase.helmholtz.cloud/<your-user>/dasf-messaging-typescript.git
```

and change directories:

```bash
cd dasf-messaging-typescript
```

You have now two options how to start the development: with or without docker.

[fork]: https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/-/forks/new

### Option 1: Development setup with docker

When you have docker available, you can start the development via the docker
compose setup in this repo with

```bash
docker compose -f docker-compose.yml -f tests/test-backend/docker-compose.dev.yml up --build
```

This will start one container with the message broker, one container with the
test backend module and one container to serve the `index.html` in the root of
the repository (in development mode). You can access the message broker at
http://localhost:8080/admin and the webfrontend via http://localhost:5173/.

### Option 2: Development setup without docker

If you do not have docker available, no worries, you can run anything locally.
First setup your virtual environment and activate it

```bash
make venv-install
source venv/bin/activate # (or venv/Scripts/Activate.bat on windows)
```

Start the message broker:

```bash
cd tests/test-backend/django
bash app.sh  # on windows, just execute the three commands in app.sh manually
```

You can access the message broker now at http://localhost:8080/admin.

Open another terminal and connect the backend module via

```bash
cd tests/test-backend
DE_BACKEND_WEBSOCKET_URL=ws://localhost:8080/ws bash app.sh
```

now open a third terminal and start the frontend development server via

```bash
npm run dev
```

You can connect to the web frontend now at http://localhost:5173/

## Running the test suite

On every push to gitlab, we will run the test suite for the package. If you
want to run the tests locally, we are providing two options again: with and
without docker.

### Option 1: Run tests with docker

Just execute the following command from the root of the repository:

```bash
docker compose -f docker-compose.test.yml -f tests/test-backend/docker-compose.dev.yml up --exit-code-from tests
```

It will start the necessary containers, run the test suite and stop everything
afterwards.

### Option 2: Run tests without docker

As described above, start the message broker and connect the backend module (see
_Option 2: Development setup without docker_ above). Now open another terminal,
cd to the root of the repository and run the test suite via

```bash
make test
```

## Helpers

### Shortcuts with make

There are several shortcuts available with the `Makefile` in the root of the
repository. On Linux, you can execute `make help` to get an overview.

### Annotating licenses

If you want to create new files, you need to set license and copyright
statements correctly. We use `reuse` to check that the licenses are
correctly encoded. As a helper script, you can use the `reuse-shortcuts` command
from the [`reuse-shortcuts` package][reuse-shortcuts] that provides several
shortcuts from `.reuse/shortcuts.yaml`. Please select the correct shortcut,
namely

- If you create a new python file, you should run

  ```bash
  reuse-shortcuts code <file-you-created>.py
  ```

- If you created a new file for the docs, you should run

  ```bash
  reuse-shortcuts docs <file-you-created>.py
  ```

- If you created any other non-code file, you should run

  ```bash
  reuse-shortcuts supp <file-you-created>.py
  ```

If you have any questions on how licenses are handled, please do not hesitate
to contact the maintainers of _dasf-messaging-typescript_.

[reuse-shortcuts]: https://pypi.org/project/reuse-shortcuts/

## Fixing the docs

The documentation for this package is written in restructured Text and
built with [sphinx](https://www.sphinx-doc.org) and deployed on
[readthedocs](https://readthedocs.org).

If you found something in the docs that you want to fix, head over to
the `docs` folder, install the necessary requirements via
`pip install -r requirements.txt` and build the docs with `make html` (or
`make.bat` on windows). The docs are then available in
`docs/_build/html/index.html` that you can open with your local browser.

Implement your fixes in the corresponding `.md`-file and push them to
your fork on gitlab.

## Contributing to the code

We use automated formatters (see their config in `.pre-commit-config.yaml`),
namely

- [prettier](https://github.com/prettier/prettier) for standardized
  code formatting
- [eslint](https://github.com/eslint/eslint) for linting the code
- [reuse](https://reuse.readthedocs.io/) for handling of licenses
- [cffconvert](https://github.com/citation-file-format/cff-converter-python)
  for validating the `CITATION.cff` file.

We highly recommend that you setup [pre-commit hooks](https://pre-commit.com/)
to automatically run all the above tools every time you make a git commit. This
can be done by running:

```bash
pre-commit install
```

from the root of the repository. You can skip the pre-commit checks with
`git commit --no-verify` but note that the CI will fail if it encounters
any formatting errors.

You can also run the `pre-commit` step manually by invoking:

```bash
pre-commit run --all-files
```

## Updating the skeleton for this package

This package has been generated from the template
https://codebase.helmholtz.cloud/hcdc/software-templates/typescript-package-template.git.

See the template repository for instructions on how to update the skeleton for
this package.
