<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(installation)=

# Installation

To install the _dasf-messaging-typescript_ package, we
recommend that you install it via the `npm` package manager

```bash
npm install '@dasf/dasf-messaging'
```

Or install it directly from [the source code repository on
Gitlab][source code repository]. Make sure you have `python>=3.10`, `npm` and
`make` installed and run

```bash
git clone https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript
cd dasf-messaging-typescript
make dev-install
```

The latter should however only be done if you want to access the
development versions.

[source code repository]: https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript

(install-develop)=

## Installation for development

Please head over to our
{ref}`contributing guide <contributing>` for
installation instruction for development.
