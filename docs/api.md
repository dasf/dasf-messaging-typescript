<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(api)=

# API Reference

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of dasf-messaging-typescript! Stay tuned for
updates and discuss with us at <https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript>
```

```{contents} Classes
---
local: true
backlinks: top
---

```

## `DASFConnection`

The central class for the DASF framework, the connection that is used to create
and send messages, and handle their response.

```{eval-rst}
.. autoclass:: DASFConnection
    :members: connected, sendMessage, sendRequest, close, getApiInfo, getModuleInfo
```

## `DASFUrlBuilder`

A class to build urls to let the {class}`DASFConnection` connect to a message
broker. We have two implemented, the {class}`WebsocketUrlBuilder`
and the {class}`PulsarUrlBuilder`.

```{eval-rst}
.. autoclass:: DASFUrlBuilder
    :members:
.. autoclass:: WebsocketUrlBuilder
    :members:
.. autoclass:: PulsarUrlBuilder
    :members:
```

## Request Message classes

The following classes represent the messages that are communicated between the
client and server stub.

```{eval-rst}
.. autoclass:: DASFModuleRequest
    :members:
.. autoclass:: DASFModuleResponse
    :members:
.. autoclass:: DASFProgressReport
    :members:
```

## Data classes

The following classes are relevant to the `ApiInfo`, see
{meth}`DASFConnection.getApiInfo`.

```{eval-rst}
.. autoclass:: ModuleApiInfo
    :members:
.. autoclass:: FunctionApiInfo
    :members:
.. autoclass:: ClassApiInfo
    :members:
```

## Enums

<!-- sphinx-js does not support something like `autoenum`, so we need to
literalinclude them here. to make them referencable, we use the `name` attribute
and the `caption` in the literalinclude statement -->

Enums are essentially the same as for the python `demessaging` package, namely
we define the {external:py:class}`~demessaging.messaging.constants.Status` enum,

```{literalinclude} ../src/messages.ts
---
start-at: export enum Status
language: typescript
name: Status
caption: Status
end-at: "}"
---
```

{external:py:class}`~demessaging.messaging.constants.PropertyKeys`

```{literalinclude} ../src/messages.ts
---
start-at: export enum PropertyKeys
language: typescript
name: PropertyKeys
end-at: "}"
caption: PropertyKeys
---
```

and {external:py:class}`~demessaging.messaging.constants.MessageType`

```{literalinclude} ../src/messages.ts
---
start-at: export enum MessageType
language: typescript
name: MessageType
caption: MessageType
end-at: "}"
---
```

{ref}`propertykeys`
