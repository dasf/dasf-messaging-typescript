<!--
SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(usage)=

# Usage

After {ref}`installation <installation>` , you can use this library to connect
to a message broker and send requests to a DASF Backend module. A common use
case is to the the version info via

```typescript
import { DASFConnection, WebsocketUrlBuilder } from '@dasf/dasf-messaging';
const connection = new DASFConnection(
  new WebsocketUrlBuilder(
    'ws://localhost:8080/ws', // adapt this to your message brokers websocket url
    'some-topic', // adapt this to the topic you can to connect to
  ),
);
```

You can then send requests via

```typescript
connection
  .sendRequest({ func_name: 'version_info' })
  .then((response) => console.log(response));
```

the {meth}`~DASFConnection.sendRequest` method here returns a promise. When
resolved, you get the response from the the backend module that you can then
handle in your application (in the above demo, we just log it to the console).

More information on the connection can be found in the API reference for the
{class}`DASFConnection`.
