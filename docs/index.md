<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

<!--
dasf-messaging-typescript documentation master file
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.
-->

# Welcome to dasf-messaging-typescript's documentation!

[![CI](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/-/graphs/main/charts)
[![Docs](https://readthedocs.org/projects/dasf-messaging-typescript/badge/?version=latest)](https://dasf.readthedocs.io/projects/typescript/en/latest/)
[![Latest Release](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/-/badges/release.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript)
[![NPM version](https://img.shields.io/npm/v/@dasf/dasf-messaging.svg)](https://www.npmjs.com/package/@dasf/dasf-messaging/)
[![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/dasf/dasf-messaging-typescript)](https://api.reuse.software/info/codebase.helmholtz.cloud/dasf/dasf-messaging-typescript)

**Typescript bindings for the DASF RPC messaging protocol.**

```{toctree}
---
maxdepth: 2
caption: "Contents:"
---
installation
usage
api
contributing
```

## How to cite this software

```{eval-rst}
.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff
```

# License information

Copyright © 2024 Helmholtz-Zentrum hereon GmbH
Copyright © 2022-2024 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Potsdam, Germany

The source code of dasf-messaging-typescript is licensed under Apache-2.0.

If not stated otherwise, the contents of this documentation is licensed
under CC-BY-4.0.

## Indices and tables

- {ref}`genindex`
- {ref}`modindex`
- {ref}`search`
