// SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: CC0-1.0

export default {
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleNameMapper: {
    '^src/(.*)$': '<rootDir>/src/$1',
  },
  setupFilesAfterEnv: ['<rootDir>/tests/setupTests.ts'],
  modulePathIgnorePatterns: ['./dist/'],
  coveragePathIgnorePatterns: [],
  collectCoverageFrom: ['./src/**/*.ts', '!./tests/**/*.test.ts'],
  coverageReporters: [
    'html',
    'clover',
    'json',
    'lcov',
    'text',
    'text-summary',
    'cobertura',
  ],
  coverageThreshold: {
    // global: {
    //   branches: 100,
    //   functions: 100,
    //   lines: 100,
    //   statements: 100,
    // },
  },
};
