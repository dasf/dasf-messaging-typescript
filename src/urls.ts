// SPDX-FileCopyrightText: 2022-2024 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Potsdam, Germany
// SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: Apache-2.0

/**
 * Interface providing DASF connection urls and parameters
 */
export interface DASFUrlBuilder {
  /**
   * Which topic is used?
   */
  topic: string;
  /**
   * The topic on that the consumer listens.
   * Needed to get the results for the backend module.
   */
  consumeTopic: string;

  /**
   * A url that allows to create a consumer with the DASF
   * web socket api.
   */
  DASFConsumerURL: string;

  /** See DASFConsumerURL for an explanation. */
  DASFProducerURL: string;
}

enum Type {
  producer = 'producer',
  consumer = 'consumer',
}

/**
 * Url builder for predefined websocket url
 */
export class WebsocketUrlBuilder implements DASFUrlBuilder {
  readonly topic: string;
  readonly consumeTopic: string;
  readonly DASFConsumerURL: string;
  readonly DASFProducerURL: string;

  constructor(
    websocketUrl: string,
    topic: string,
    producerUrl = '',
    consumerUrl = '',
  ) {
    this.topic = topic;
    this.consumeTopic = this.generateConsumeTopic(topic);
    this.DASFConsumerURL =
      (consumerUrl
        ? consumerUrl
        : this.buildUrl(websocketUrl, this.consumeTopic)) +
      '/' +
      this.generateSubscriptionPrefix();
    this.DASFProducerURL = producerUrl
      ? producerUrl
      : this.buildUrl(websocketUrl, this.topic);
  }

  protected buildUrl(websocketUrl: string, topic: string): string {
    return websocketUrl + '/' + topic;
  }

  protected generateConsumeTopic(topic: string): string {
    return topic + '_' + this.generateRequestToken();
  }

  protected generateSubscriptionPrefix(): string {
    return 'web-frontend-' + new Date().toISOString();
  }

  protected generateRequestToken(): string {
    return Math.random().toString(16).slice(2);
  }
}

/**
 * Default url builder for apache pulsar implementation based on host:port, namespace and topic
 */
export class PulsarUrlBuilder implements DASFUrlBuilder {
  private static readonly BASE_DASF_URL: string =
    '%protocol%://%host%:%port%/ws/v2/%type%/non-persistent/public/%namespace%/%topic%/';

  readonly topic: string;
  readonly consumeTopic: string;
  readonly DASFConsumerURL: string;
  readonly DASFProducerURL: string;

  constructor(
    host: string,
    port: string,
    namespace: string,
    topic: string,
    protocol: string = 'ws',
  ) {
    this.topic = topic;
    this.consumeTopic = this.generateConsumeTopic(topic);
    this.DASFConsumerURL = this.buildUrl(
      host,
      port,
      namespace,
      Type.consumer,
      this.consumeTopic,
      protocol,
    );
    this.DASFProducerURL = this.buildUrl(
      host,
      port,
      namespace,
      Type.producer,
      this.topic,
      protocol,
    );
  }

  protected buildUrl(
    host: string,
    port: string,
    namespace: string,
    type: Type,
    topic: string,
    protocol: string,
  ): string {
    let url = PulsarUrlBuilder.BASE_DASF_URL.replace('%host%', host)
      .replace('%port%', port)
      .replace('%namespace%', namespace)
      .replace('%type%', type)
      .replace('%topic%', topic)
      .replace('%protocol%', protocol);

    if (type == Type.consumer) {
      // append subscription
      url += this.generateSubscriptionPrefix() + '?subscriptionType=Exclusive';
    }

    return url;
  }

  protected generateConsumeTopic(topic: string): string {
    return topic + '_' + this.generateRequestToken();
  }

  protected generateSubscriptionPrefix(): string {
    return 'web-frontend-' + new Date().toISOString();
  }

  protected generateRequestToken(): string {
    return Math.random().toString(16).slice(2);
  }
}
