# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

authors:
  # list author information. see
  # https://github.com/citation-file-format/citation-file-format/blob/main/schema-guide.md#definitionsperson
  # for metadata
  - family-names: 'Eggert'
    given-names: 'Daniel'
    affiliation: 'Helmholtz-Zentrum Hereon'
    # orcid: null
  - family-names: 'Sommer'
    given-names: 'Philipp S.'
    affiliation: 'Helmholtz-Zentrum Hereon'
    # orcid: null
    email: 'philipp.sommer@hereon.de'
    website: 'https://www.philipp-s-sommer.de/'
cff-version: 1.2.0
message: 'If you use this software, please cite it using these metadata.'
title: 'DASF Messaging library for Javascript and Typescript'
keywords:
  - 'digital earth'
  - 'helmholtz'
  - 'gfz'
  - 'potsdam'
  - 'dasf'
  - 'data analytics'
  - 'typescript'
  - 'messaging'
  - 'rpc'

license: 'Apache-2.0'
repository-code: 'https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript'
url: 'https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript'
contact:
  # list maintainer information. see
  # https://github.com/citation-file-format/citation-file-format/blob/main/schema-guide.md#definitionsperson
  # for metadata
  - family-names: 'Sommer'
    given-names: 'Philipp S.'
    affiliation: 'Helmholtz-Zentrum Hereon'
    # orcid: null
    email: 'philipp.sommer@hereon.de'
    website: 'https://www.philipp-s-sommer.de/'
abstract: |
  Typescript bindings for the DASF RPC messaging protocol.
preferred-citation:
  title: 'DASF: A data analytics software framework for distributed environments'
  authors:
    - family-names: Eggert
      given-names: 'Daniel'
      affiliation: 'Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences'
      orcid: 'https://orcid.org/0000-0003-0251-4390'
    - family-names: Sips
      given-names: 'Mike'
      affiliation: 'Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences'
      orcid: 'https://orcid.org/0000-0003-3941-7092'
    - family-names: Sommer
      given-names: 'Philipp S.'
      affiliation: 'Helmholtz-Zentrum Hereon'
      orcid: 'https://orcid.org/0000-0001-6171-7716'
    - family-names: Dransch
      given-names: 'Doris'
      affiliation: 'https://orcid.org/0000-0003-3941-7092'
  year: 2022
  type: article
  doi: '10.21105/joss.04052'
  date-published: 2022-10-13
  journal: Journal of Open Source Software
  volume: 7
  number: 78
  pages: 4052
  publisher:
    name: The Open Journal
  license: CC-BY-4.0
