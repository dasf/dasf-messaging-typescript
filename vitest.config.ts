// SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: CC0-1.0
// SPDX-License-Identifier: CC0-1.0

import { fileURLToPath } from 'node:url';
import { mergeConfig, defineConfig, configDefaults } from 'vitest/config';
import { loadEnv } from 'vite';
import viteConfig from './vite.config';

export default mergeConfig(
  viteConfig,
  defineConfig({
    test: {
      environment: 'jsdom',
      exclude: [...configDefaults.exclude, 'e2e/**'],
      root: fileURLToPath(new URL('./', import.meta.url)),
      globals: true,
      env: loadEnv(process.cwd(), ''),
      coverage: {
        reportsDirectory: './coverage/reports',
        exclude: [...configDefaults.coverage.exclude, 'venv', 'docs'],
        reporter: [
          'html',
          'clover',
          'json',
          'lcov',
          'text',
          'text-summary',
          'cobertura',
          'json-summary',
        ],
      },
    },
  }),
);
