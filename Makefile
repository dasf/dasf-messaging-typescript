# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

.PHONY: clean clean-build clean-pyc clean-test coverage dist docs help dev-install lint
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys

from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-modules ## remove all build, virtual environments, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr dist/
	rm -fr build/

clean-modules: ## remove node modules artifacts
	rm -fr node_modules/

lint/eslint: ## check style with eslint
	npm run lint

lint/reuse:  ## check licenses
	reuse lint

lint: lint/eslint lint/reuse ## check style

formatting:
	npm run format

test: ## run tox
	npm test -- --coverage

docs: ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

test-docs: ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs linkcheck

servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.md;*.ts' -c '$(MAKE) -C docs html' -R -D .

release: dist ## package and upload a release
	npm publish dist/*

dist: clean-build ## builds source and wheel package
	npm run build
	mkdir build
	npm pack --pack-destination build/
	ls -l dist

dev-install: clean ## install the necessary dependencies
	npm install
	python -m pip install -r requirements.dev.txt -r tests/test-backend/django/requirements.txt -r tests/test-backend/requirements.txt
	pre-commit install

venv-install: clean ## install the necessary dependencies
	npm install
	python -m venv venv
	venv/bin/python -m pip install -r requirements.dev.txt -r tests/test-backend/django/requirements.txt -r tests/test-backend/requirements.txt
	venv/bin/pre-commit install
