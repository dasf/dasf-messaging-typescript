// SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: Apache-2.0
import { describe, it, expect } from 'vitest';

import { DASFConnection, WebsocketUrlBuilder } from '../src/main';

function createConnection(uri?: string): DASFConnection {
  const websocketUrl = uri
    ? uri
    : import.meta.env.VITE_WEBSOCKET_URL || 'ws://localhost:8080/ws';
  const topic = 'mytesttopic';
  return new DASFConnection(new WebsocketUrlBuilder(websocketUrl, topic));
}

describe.concurrent('DASFConnection', () => {
  it('DASFConnection | getModuleInfo', async () => {
    const connection = createConnection();
    // eslint-disable-next-line vitest/valid-expect
    expect(connection.getModuleInfo()).resolves.toHaveProperty('title');
  });

  it('DASFConnection | getApiInfo', async () => {
    const connection = createConnection();
    // eslint-disable-next-line vitest/valid-expect
    expect(connection.getApiInfo()).resolves.toHaveProperty('functions');
  });

  it('DASFConnection | sendRequest', async () => {
    const connection = createConnection();
    const requestData = {
      func_name: 'test_function',
    };
    // eslint-disable-next-line vitest/valid-expect
    expect(connection.sendRequest(requestData)).resolves.toBe(1);
  });

  it('DASFConnection | connected', async () => {
    const connection = createConnection();
    await expect(connection.connected).resolves.toBe(connection);
  });

  it('DASFConnection | connectionError', async () => {
    const connection = createConnection('ws://localhost:8121/failure');
    await expect(connection.connected).rejects.toThrowError(
      'Connection to ws://localhost:8121/failure/mytesttopic failed',
    );
  });

  it('DASFConnection | sendRequest with Progress', async () => {
    const connection = createConnection();
    const requestData = {
      func_name: 'test_function_progress_report',
    };
    const progressData = [];

    const logProgress = (log) => {
      progressData.push(log);
    };

    // eslint-disable-next-line vitest/valid-expect
    expect(connection.sendRequest(requestData, logProgress))
      .resolves.toBe(1)
      .then(() => {
        expect(progressData[progressData.length - 1].message.status).toBe(
          'success',
        );
      });
  });
});
