// SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
//
// SPDX-License-Identifier: Apache-2.0
import { describe, it, expect } from 'vitest';

import { WebsocketUrlBuilder } from '../src/urls';

// import { helloWorld } from '../src/main';

describe('WebsocketUrlBuilder', () => {
  it('WebsocketUrlBuilder | DASFProducerURL', async () => {
    const urlBuilder = new WebsocketUrlBuilder(
      'ws://somewhere.com/ws',
      'sometopic',
    );
    expect(urlBuilder.DASFProducerURL).toBe('ws://somewhere.com/ws/sometopic');
  });
  it('WebsocketUrlBuilder | DASFConsumerURL', async () => {
    const urlBuilder = new WebsocketUrlBuilder(
      'ws://somewhere.com/ws',
      'sometopic',
    );
    expect(urlBuilder.DASFConsumerURL).toMatch(
      new RegExp('ws://somewhere.com/ws/sometopic_\\w+/web-frontend-.+'),
    );
  });
});
