# SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

python manage.py migrate

python manage.py dasf_topic -n mytesttopic --anonymous

python manage.py runserver 0.0.0.0:8080
