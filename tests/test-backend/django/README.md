<!--
SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: Apache-2.0
-->

# DASF message broker for local development

This django project can be used for local development.
