# SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

"""Backend module for test-backend."""

import os
from typing import Dict, Optional

from demessaging import configure, main, registry  # noqa: F401
from deprogressapi import ProgressReport

__all__ = ["version_info", "test_function", "test_function_progress_report"]


def version_info() -> Dict[str, str]:
    """Get the version of extpar and extpar_client."""
    import test_module

    info = {
        "test-backend": test_module.__version__,
    }
    return info


def test_function() -> int:
    """Some simple test function."""
    return 1


def test_function_progress_report(
    reporter: Optional[ProgressReport] = ProgressReport(
        step_message="Demo report"
    ),
) -> int:
    """Demo function for a progress report with the dasf-progress-api."""
    import time

    reporter = ProgressReport.from_arg(reporter)

    num_steps = 3
    reporter.steps = num_steps

    with reporter:
        for i in range(num_steps):
            with reporter.create_subreport(step_message=f"Sub report {i}"):
                time.sleep(1)
    return 1


if __name__ == "__main__":
    if os.getenv("DE_MESSAGING_ENV_FILE"):
        from demessaging.config import BaseMessagingConfig, ModuleConfig

        config = ModuleConfig(
            messaging_config=BaseMessagingConfig(  # type: ignore[arg-type]
                topic="mytesttopic",
                _env_file=os.environ["DE_MESSAGING_ENV_FILE"],
            )
        )
        main(config=config)
    else:
        main(messaging_config=dict(topic="mytesttopic"))
