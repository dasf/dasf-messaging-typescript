# SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

"""
Backend module for test-backend.
"""
from typing import Callable, Dict

from demessaging import BackendModule as _BackendModule
from demessaging import main
from demessaging.config import ModuleConfig

NoneType = type(None)


__all__ = ["version_info"]


def version_info() -> Dict[str, str]:
    """
    Get the version of extpar and extpar_client.
    """
    request = {
        "func_name": "version_info",
    }

    model = BackendModule.model_validate(request)
    response = model.compute()

    return response.root  # type: ignore


backend_config = ModuleConfig.model_validate_json(
    """
{
    "messaging_config": {
        "topic": "mytesttopic",
        "max_workers": null,
        "queue_size": null,
        "max_payload_size": 512000,
        "producer_keep_alive": 120,
        "producer_connection_timeout": 30,
        "websocket_url": "ws://localhost:8000/ws/",
        "producer_url": "",
        "consumer_url": ""
}
}
"""
)

_creator: Callable
if __name__ == "__main__":
    _creator = main
else:
    _creator = _BackendModule.create_model

BackendModule = _creator(
    __name__,
    config=backend_config,
    class_name="BackendModule",
    members=[version_info],
)
