# SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

"""DASF Messaging library for Javascript and Typescript

Typescript bindings for the DASF RPC messaging protocol.
"""

from __future__ import annotations

__version__ = "0.0.1"

__author__ = "Philipp S. Sommer"
__copyright__ = "2025 Helmholtz-Zentrum hereon GmbH"
__credits__ = [
    "Philipp S. Sommer",
]
__license__ = "Apache-2.0"

__maintainer__ = "Philipp S. Sommer"
__email__ = "philipp.sommer@hereon.de"

__status__ = "Pre-Alpha"
