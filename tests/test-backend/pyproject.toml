# SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

[build-system]
build-backend = 'setuptools.build_meta'
requires = ['setuptools >= 61.0']

[project]
name = "test-backend"
version = "0.0.1"
description = "Typescript bindings for the DASF RPC messaging protocol."

readme = "README.md"
keywords = [
    "digital earth",
    "helmholtz",
    "gfz",
    "potsdam",
    "dasf",
    "data analytics",
    "typescript",
    "messaging",
    "rpc",
]

authors = [
    { name = 'Philipp S. Sommer', email = 'philipp.sommer@hereon.de' },
]
maintainers = [
    { name = 'Philipp S. Sommer', email = 'philipp.sommer@hereon.de' },
]
license = { text = 'Apache-2.0' }

classifiers = [
    "Intended Audience :: Developers",
    "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Typing :: Typed",
]

requires-python = '>= 3.9'
dependencies = [
    "demessaging>=0.4.0",
    # add your dependencies here
]

[project.urls]
Homepage = 'https://codebase.helmholtz.cloud/dasf/test-backend'
Documentation = "https://dasf.readthedocs.io/projects/typescript/en/latest/"
Source = "https://codebase.helmholtz.cloud/dasf/test-backend"
Tracker = "https://codebase.helmholtz.cloud/dasf/test-backend/issues/"


[project.optional-dependencies]
backend = ["demessaging[backend]"]
testsite = [
    "test-backend[backend]",
    "tox",
    "isort==5.12.0",
    "black==23.1.0",
    "blackdoc==0.3.8",
    "flake8==6.0.0",
    "pre-commit",
    "mypy",
    "dasf-broker-django",
    "djangorestframework",
    "pytest-django",
    "pytest-cov",
    "reuse",
    "cffconvert",

]
docs = [
    "autodocsumm",
    "sphinx-rtd-theme",
    "hereon-netcdf-sphinxext",
    "sphinx-design",
    "myst_parser",
]
dev = [
    "test-backend[testsite]",
    "test-backend[docs]",
    "reuse-shortcuts>=1.0.1",
    "types-PyYAML",
]


[tool.mypy]
ignore_missing_imports = true
plugins = [
    "pydantic.mypy",
]

[tool.setuptools]
zip-safe = false
license-files = ["LICENSES/*"]

[tool.setuptools.package-data]
test_module = ["py.typed"]

[tool.setuptools.packages.find]
namespaces = false
exclude = [
    'docs',
    'tests*',
    'examples'
]

[tool.pytest.ini_options]
addopts = '-v'
DJANGO_SETTINGS_MODULE = "testproject.settings"

[tool.isort]
profile = "black"
line_length = 79
src_paths = ["test_module"]
float_to_top = true
known_first_party = "test_module"

[tool.black]
line-length = 79
target-version = ['py39']

[tool.coverage.run]
omit = ["test_module/_version.py"]
