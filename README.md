<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-FileCopyrightText: 2022-2024 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Potsdam, Germany

SPDX-License-Identifier: CC-BY-4.0
-->

# DASF Messaging library for Javascript and Typescript

[![CI](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/-/graphs/main/charts)
[![Docs](https://readthedocs.org/projects/dasf-messaging-typescript/badge/?version=latest)](https://dasf.readthedocs.io/projects/typescript/en/latest/)
[![Latest Release](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript/-/badges/release.svg)](https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript)
[![NPM version](https://img.shields.io/npm/v/@dasf/dasf-messaging.svg)](https://www.npmjs.com/package/@dasf/dasf-messaging/)
[![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/dasf/dasf-messaging-typescript)](https://api.reuse.software/info/codebase.helmholtz.cloud/dasf/dasf-messaging-typescript)

**Typescript bindings for the DASF RPC messaging protocol.**

`dasf-messaging-typescript` is part of the Data Analytics Software Framework (DASF, https://git.geomar.de/digital-earth/dasf),
developed at the GFZ German Research Centre for Geosciences (https://www.gfz-potsdam.de).
It is funded by the Initiative and Networking Fund of the Helmholtz Association through the Digital Earth project
(https://www.digitalearth-hgf.de/).

`dasf-messaging-typescript` provides the typescript bindings for the DASF RPC messaging protocol and therefore allows to connect to any algorithm or method (e.g. via the `dasf-messaging-python` implementation). Because of the component based architecture the integrated method could be deployed anywhere (e.g. close to the data it is processing). You may use `dasf-web` for interactive data visualizations.

## Installation and usage

Install this package to your `package.json` via

```bash
npm install '@dasf/dasf-messaging'
```

you can then use this library to create a `DASFConnection`

```typescript
import { DASFConnection, WebsocketUrlBuilder } from '@dasf/dasf-messaging';
const connection = new DASFConnection(
  new WebsocketUrlBuilder(
    'ws://localhost:8080/ws', // adapt this to your message brokers websocket url
    'some-topic', // adapt this to the topic you can to connect to
  ),
);
```

and send requests via

```typescript
connection
  .sendRequest({ func_name: 'version_info' })
  .then((response) => console.log(response)); // do something with the response
```

More detailed instructions on installation, usage, API and development can be
found in the [docs][docs].

[source code]: https://codebase.helmholtz.cloud/dasf/dasf-messaging-typescript
[docs]: https://dasf.readthedocs.io/projects/typescript/en/latest/installation.html

## Technical note

This package has been generated from the template
https://codebase.helmholtz.cloud/hcdc/software-templates/typescript-package-template.git.

See the template repository for instructions on how to update the skeleton for
this package.

### Recommended Software Citation

`Eggert et al., (2022). DASF: A data analytics software framework for distributed environments. Journal of Open Source Software, 7(78), 4052, https://doi.org/10.21105/joss.04052`

## License information

Copyright © 2024 Helmholtz-Zentrum hereon GmbH
Copyright © 2022-2024 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Potsdam, Germany

Code files in this repository are licensed under the Apache-2.0, if not stated otherwise in the file.

Documentation files in this repository are licensed under CC-BY-4.0, if not stated otherwise in the file.

Supplementary and configuration files in this repository are licensed
under CC0-1.0, if not stated otherwise
in the file.

Please check the header of the individual files for more detailed
information.

### License management

License management is handled with [`reuse`](https://reuse.readthedocs.io/).
If you have any questions on this, please have a look into the
[contributing guide][contributing] or contact the maintainers of
`dasf-messaging-typescript`.

[contributing]: https://dasf.readthedocs.io/projects/typescript/en/latest/contributing.html
