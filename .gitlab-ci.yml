# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

image: registry.access.redhat.com/ubi9/python-311

variables:
  PIP_CACHE_DIR: '$CI_PROJECT_DIR/.cache/pip'

cache:
  paths:
    - .cache/pip

.python_setup:
  before_script:
    - python -m pip install -U pip
    - git config --global --add safe.directory ${CI_PROJECT_DIR}

.dedicated-runner: &dedicated-runner
  tags:
    - dind

build-package:
  extends: .python_setup
  stage: build
  script:
    - make dev-install
    - make dist
  artifacts:
    name: package-artifacts
    when: always
    paths:
      - 'dist/*'
      - 'build/*'
    expire_in: 7 days

lint:
  extends: .python_setup
  stage: test
  script:
    - make dev-install
    - make lint

test:
  <<: *dedicated-runner
  stage: test
  image: docker:20.10
  services:
    - docker:20.10-dind
  script:
    - docker compose -f docker-compose.test.yml -f tests/test-backend/docker-compose.dev.yml build
    - docker compose -f docker-compose.test.yml -f tests/test-backend/docker-compose.dev.yml up --exit-code-from tests
  coverage: '/Lines\s*:\s*(\d+.?\d*)%/'
  needs:
    - build-package
    - lint
  artifacts:
    name: coverage-artifacts
    when: always
    paths:
      - 'coverage/*'
    expire_in: 7 days

test-docs:
  extends: .python_setup
  stage: test
  script:
    - make dev-install
    - make -C docs html
    - make -C docs linkcheck
  artifacts:
    when: always
    paths:
      - docs/_build
  needs:
    - build-package

deploy-package:
  stage: deploy
  needs:
    - build-package
    - test-docs
    - test
  only:
    - tags
  script:
    - echo "@dasf:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/" > .npmrc
    - echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
    - npm publish

deploy-package-to-npm:
  stage: deploy
  needs:
    - build-package
    - test-docs
    - test
  only:
    - tags
  script:
    - echo "@dasf:registry=https://registry.npmjs.org/" > .npmrc
    - echo "//registry.npmjs.org/:_authToken=${NPM_ACCESS_TOKEN}" >> .npmrc
    - npm publish --access public
