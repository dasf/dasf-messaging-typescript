# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

FROM node:22-bookworm AS builder
WORKDIR /app
COPY . ./
RUN \
    apt update && \
    apt install -y git && \
    NPM_TOKEN= npm ci
EXPOSE 5173
ENTRYPOINT npm run dev -- --host

FROM node:22-alpine AS runner
WORKDIR /app
COPY --from=builder /app .
EXPOSE 5173
ENTRYPOINT npm run dev -- --host
